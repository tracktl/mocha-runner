mocha-runner
============

_Node.js project_

#### Runs Mocha tests locally without using makefiles (Windows supported) ####

Version: 0.0.4

If you simply want to run Mocha tests without installing it globally and without using makefiles -therefore both Windows and Linux are supported- this library is made for you.

Furthermore, for ease the testing process the directory from where you run the tests is cleaned before executing them.

<table>
<tr>
	<th colspan="2">package.json</th>
</tr>
<tr>
	<th>Before (Linux only)</th>
	<th>After (Linux & Windows)</th>
</tr>
<tr>
	<td>
		"scripts" : {<br/>
			&emsp;"test" : "make test"<br/>
		}
	</td>
	<td>
		"scripts" : {<br/>
			&emsp;"test" : "node tests/run.js"<br/>
		}
	</td>
</tr>
</table>

#### Installation ####

```
npm install mocha-runner
```

#### Example ####

```javascript
var Runner = require ("mocha-runner");

new Runner ({
	//If the reporter is not specified "dot" is used by default
	reporter: "list",
	
	//Before running the tests all the files are removed to ensure a consistent state except:
	//- Excluded files
	//- Test files
	//- node_modules directory
	//- This file (the file that uses mocha-runner to run the tests)
	//It's recommended to create a folder named `tests` and put there all the testing stuff including this file
	exclude: ["file1", "file2"],
	
	//To remove some specific files
	//If this property is set, "exclude" is ignored
	include: ["file1", "file2"],
	
	tests: ["test1", "test2"]
}).run (function (error){
	//It's not the Mocha stderr
	if (error) console.log (error);
});
```
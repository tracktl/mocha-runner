"use strict";

var ep = require ("error-provider");
var fs = require ("fs");
var path = require ("flavored-path");
var Mocha = require ("mocha");
var rmrf = require ("./rmrf");

ep.create (ep.next (), "NO_TESTS", "No tests to run");

var endsWith = function (str, suffix){
	return str.indexOf (suffix, str.length - suffix.length) !== -1;
};

var Runner = module.exports = function (args){
	var tests = args.tests || [];
	if (!tests.length) throw ep.get ("NO_TESTS");
	
	this._mocha = new Mocha ({
		reporter: args.reporter || "dot"
	});
	
	var fixedTests = [];
	
	for (var i=0, len=tests.length; i<len; i++){
		if (!endsWith (tests[i], ".js")){
			tests[i] += ".js";
		}
		var p = path.get (tests[i]);
		this._mocha.addFile (p);
		fixedTests.push (p);
	}
	
	var fix = function (array){
		var arr = [];
		array.forEach (function (s){
			arr.push (path.get (s))
		});
		return arr;
	};
	
	this._exclude = fix (args.exclude || []);
	this._include = fix (args.include || []);
	this._exclude = this._exclude.concat (fixedTests, path.get ("node_modules"),
			path.get (path.basename (process.mainModule.filename)));
};

Runner.prototype._clean = function (cb){
	if (this._include.length){
		var remaining = this._include.length;
		if (!remaining) return cb (null);
		var errors = [];
		
		this._include.forEach (function (file){
			rmrf (file, function (error){
				if (errors) errors.concat (error);
				if (!--remaining) cb (errors.length ? errors : null);
			});
		});
		return;
	}
	
	var me = this;

	var needRemove = function (entry){
		for (var i=0, len=me._exclude.length; i<len; i++){
			if (me._exclude[i] === entry){
				return false;
			}
		}
		return true;
	};
	
	fs.readdir (path.get ("."), function (error, entries){
		if (error) return cb (error);
		
		var remaining = entries.length;
		var errors = [];
		if (!remaining) return cb (null);
		
		var finish = function (){
			if (!--remaining) cb (errors.length ? errors : null);
		};
		
		entries.forEach (function (entry){
			entry = path.get (entry);
			if (needRemove (entry)){
				rmrf (entry, function (error){
					if (errors) errors.concat (error);
					finish ();
				});
			}else{
				finish ();
			}
		});
	});
};

Runner.prototype.run = function (cb){
	var me = this;
	this._clean (function (error){
		if (error) return cb (error);
		me._mocha.run (function (failures){
			process.exit (failures);
		});
	});
};
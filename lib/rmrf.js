"use strict";

var fs = require ("fs");

module.exports = function (p, cb){
	var rm = function (p, cb){
		var exit = function (error){
			if (error){
				errors = errors.concat (error);
			}
			cb ();
		};
		
		fs.stat (p, function (error, stats){
			if (error) return exit (error);
			
			if (stats.isDirectory ()){
				fs.readdir (p, function (error, entries){
					if (error) return exit (error);
					
					var del = function (){
						fs.rmdir (p, exit);
					};
					
					var remaining = entries.length;
					if (!remaining) return del ();
					
					entries.forEach (function (entry){
						rm (p + "/" + entry, function (){
							if (!--remaining){
								del ();
							}
						});
					});
				});
			}else{
				fs.unlink (p, exit);
			}
		});
	};
	
	var errors = [];
	rm (p, function (){
		cb (errors.length ? errors: null);
	});
};